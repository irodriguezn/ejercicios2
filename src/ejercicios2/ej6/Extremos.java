/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios2.ej6;

/**
 *
 * @author nachorod
 */
public interface Extremos {

    /**
     *
     * @param a
     * @return
     */
    int min(int [] a);
    int max(int [] a);
    double min(double [] a);
    double max(double [] a);    
    
}
