/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicios2.ej5;

/**
 *
 * @author nachorod
 */
public class RealesApp {
    public static void main(String[] args) {
        double listaReales[]={2,2,7,4,5,6,27,8,9,10};
        ArrayReales reales=new ArrayReales(listaReales);        
        System.out.println("El mínimo es " + reales.minimo());
        System.out.println("El máximo es " + reales.maximo());
        System.out.println("El sumatorio es " + reales.sumatorio());
    }
}
