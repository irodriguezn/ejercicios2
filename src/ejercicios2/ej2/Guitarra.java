/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicios2.ej2;

/**
 *
 * @author nachorod
 */
public class Guitarra extends Instrumento {
    public Guitarra(String id) {
        super(id);
    }
    public void afinar() {
        System.out.println("ñiaaaaaaa");
    }
    
    public void hacerSonar(){
        int numEs, numNs, numAs;
        String cadena, Ees, Enes, Aes;
        int sonido=(int)(Math.random()*2+1);
        switch (sonido) {
            case 1:
                numEs=(int)(Math.random()*15+1);
                numNs=(int)(Math.random()*15+1);
                Ees=Util.repiteLetra("e",numEs);
                Enes=Util.repiteLetra("n",numNs);
                cadena="p"+Ees+"w"+Enes+"g";
                System.out.println(cadena);
                break;
            case 2:
                numAs=(int)(Math.random()*15+1);
                numNs=(int)(Math.random()*15+1);
                Aes=Util.repiteLetra("a",numAs);
                Enes=Util.repiteLetra("n",numNs);
                cadena="ch"+Aes+Enes+"g";
                System.out.println(cadena);                
        }
    }
    
    
}
