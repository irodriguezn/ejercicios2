/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicios2.ej6;

/**
 *
 * @author nachorod
 */
public final class Math3 implements Extremos {
    @Override
    public int min(int [] a) {
        int min=a[0];
        for (int el:a) {
            if (el<min) {min=el;}
        }
        return min;
    }
    
    @Override
    public int max(int [] a) {
        int max=0;
        for (int el:a) {
            if (el>max) {max=el;}
        }
        return max;
    }

    @Override
    public double min(double [] a) {
        double min=a[0];
        for (double el:a) {
            if (el<min) {min=el;}
        }
        return min;
    }

    @Override
    public double max(double [] a) {
        double max=0;
        for (double el:a) {
            if (el>max) {max=el;}
        }
        return max;
    }    
}
