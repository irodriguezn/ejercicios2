/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios2.ej5;

/**
 *
 * @author nachorod
 */
public interface Estadisticas {
    double maximo();
    double minimo();
    double sumatorio();
}
