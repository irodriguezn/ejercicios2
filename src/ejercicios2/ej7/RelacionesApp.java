/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicios2.ej7;

/**
 *
 * @author nachorod
 */
public class RelacionesApp {
    public static void main(String[] args) {
        Coche c1=new Coche(100);
        Coche c2=new Coche(50);
        Coche c3=new Coche(50);
        if (c1.esMayor(c2)) {
            System.out.println("c1 es más rápido que c2");
        } else {
            System.out.println("c1 no es más rapido que c2");
        }
        
        if (c3.esMenor(c1)) {
            System.out.println("c3 es más lento que c1");
        } else {
            System.out.println("c3 no es más lento que c1");
        }
        
        if (c2.esIgual(c3)) {
            System.out.println("c2 y c3 son igual de veloces");
        } else {
            System.out.println("c2 y c3 no son igual de veloces");
        }
    }

}
