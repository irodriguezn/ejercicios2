/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicios2.ej1;

/**
 *
 * @author nachorod
 */
abstract class ProfesorBase implements IProfesor {
    String nombre;
    
    public void setNombre(String nombre) {
        this.nombre=nombre;
    }
    
    @Override
    public void identificarse() {
        System.out.println("Me llamo " + this.nombre);
        masInfo();
    }
    
    protected abstract void masInfo();
}
