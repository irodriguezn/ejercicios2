/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios2.ej7;

/**
 *
 * @author nachorod
 */
public interface Relaciones {
    // Devuelve verdadero si a es mayor que b
    boolean esMayor(Object b);
    // Devuelve verdadero si a es menor que b
    boolean esMenor(Object b);
    // Devuelve verdadero si a es igual que b
    boolean esIgual(Object b);
}
