/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicios2.ej7;

/**
 *
 * @author nachorod
 */
public class Coche implements Relaciones {
    private int velocidad;
    
    public Coche(int v) {
        velocidad=v;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }
    
    @Override
    public boolean esMayor(Object b) {
        return velocidad>((Coche) b).getVelocidad();
    }
    
    @Override
    public boolean esMenor(Object b) {
        return velocidad<((Coche) b).getVelocidad();
    }

    @Override
    public boolean esIgual(Object b) {
        return velocidad==((Coche) b).getVelocidad();
    }

    
}
