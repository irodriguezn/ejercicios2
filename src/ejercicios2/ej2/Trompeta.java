/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicios2.ej2;

/**
 *
 * @author nachorod
 */
public class Trompeta extends Instrumento {
    public Trompeta (String id) {
        super(id);
    }
    public void afinar() {
        System.out.println("tiaaaaaauuunnnmmm tiauunnmmm");
    }
    
    public void hacerSonar(){
        int numEs, numNs, numAs;
        String cadena, Ees, Enes, Aes;
        int sonido=(int)(Math.random()*2+1);
        switch (sonido) {
            case 1:
                numAs=(int)(Math.random()*15+1);
                Aes=Util.repiteLetra("a",numAs);
                cadena="t"+Aes+"t";
                Aes=Util.repiteLetra("a",numAs);
                cadena+=Aes+"r";
                Aes=Util.repiteLetra("a",numAs);
                cadena+=Aes+"t";
                Aes=Util.repiteLetra("a",numAs);
                cadena+=Aes;
                System.out.println(cadena);
                break;
            case 2:
                numAs=(int)(Math.random()*15+1);
                Aes=Util.repiteLetra("a",numAs);
                cadena="t"+Aes+"r";
                Aes=Util.repiteLetra("a",numAs);
                cadena+=Aes+"r";
                Aes=Util.repiteLetra("a",numAs);
                cadena+=Aes;
                System.out.println(cadena);
        }
    }
}
