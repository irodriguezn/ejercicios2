/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicios2.ej2;

/**
 *
 * @author nachorod
 */
abstract class Instrumento implements IInstrumento {
    String id;

    public Instrumento(String id) {
        this.id = id;
    }
    
    @Override
    public void tocar() {
        int numVeces=(int)(Math.random()*20+1);
        for (int i=1; i<=numVeces; i++) {
            hacerSonar();
        }
    }
    
    public abstract void afinar();
    public abstract void hacerSonar();
}
