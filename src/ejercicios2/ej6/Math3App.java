/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicios2.ej6;

/**
 *
 * @author nachorod
 */
public class Math3App {
    public static void main(String[] args) {
        int enteros[]={3,2,5,6};
        double dobles[]={2,4,5,6};
        Math3 m=new Math3();
        System.out.println("El máximo del array de enteros es " + 
                m.max(enteros));
        System.out.println("El mínimo del array de enteros es " +
                m.min(enteros));
        System.out.println("El máximo del array de dobles es " + 
                m.max(dobles));
        System.out.println("El mínimo del array de dobles es " +
                m.min(dobles));        
    }
}
