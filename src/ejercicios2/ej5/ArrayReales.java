/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicios2.ej5;

/**
 *
 * @author nachorod
 */
public class ArrayReales {
    double reales[];
    
    // Creo un constructor al que le paso el número máximo
    // de elementos que tendrá el array
    public ArrayReales(int limite) {
        reales=new double[limite];
    }
    
    // Creo otro al que directamente le paso un Array de reales
    public ArrayReales(double reales[]) {
        this.reales=reales;
    }
    
    public double getElemento(int pos) {
        return reales[pos];
    }
    
    public double extraerElemento(int pos) {
        double resultado=reales[pos];
        reales[pos]=0;
        return resultado;
    }
    
    public void setElemento(double num, int pos) {
        reales[pos]=num;
    }
    
    public double minimo(){
        double min=reales[0];
        for (int i=1; i<reales.length; i++) {
            if (min>reales[i]) {min=reales[i];}
        }
        return min;
    }
    
    public double maximo(){
        double max=0;
        for (int i=0; i<reales.length; i++) {
            if (max<reales[i]) {max=reales[i];}
        }        
        return max;
    }

    public double sumatorio(){
        double suma=0;
        for (int i=0; i<reales.length; i++) {
            suma+=reales[i];
        }        
        return suma;
    }    
}
